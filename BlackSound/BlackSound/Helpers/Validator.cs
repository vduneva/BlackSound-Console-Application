﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Helpers
{
    public class Validator<T> where T : class
    {
        public bool IsValid(T item)
        {
            ValidationContext context = new ValidationContext(item);
            List<ValidationResult> results = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(item, context, results, true);

            if (!valid)
            {
                foreach (ValidationResult vr in results)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("{0}{1}", vr.ErrorMessage, Environment.NewLine);

                }
                Console.ReadKey(true);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
