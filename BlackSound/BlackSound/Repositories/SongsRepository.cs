﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repositories
{
    public class SongsRepository : BaseRepository<Song>
    {
        public SongsRepository(string filePath) : base(filePath)
        {
        }
    }
}
