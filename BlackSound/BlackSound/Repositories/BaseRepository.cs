﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using BlackSound.Entities;

namespace BlackSound.Repositories
{
    public class BaseRepository<T> where T : Entities.BaseEntity, new()
    {
        protected readonly string filePath;

        public BaseRepository(string filePath)
        {
            this.filePath = filePath;
        }

        private int GetNextId()
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            int id = 1;
            try
            {
                while (!sr.EndOfStream)
                {
                    List<T> items = DeserializeList(sr.ReadToEnd());
                    if (items.Count > 0)
                    {
                        T lastItem = items[items.Count - 1];
                        id = lastItem.ID + 1;
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return id;
        }

        public T GetById(int id)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    List<T> result = DeserializeList(sr.ReadToEnd());
                    T temp = result.FirstOrDefault(s => s.ID == id);

                    return temp;
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }

        public List<T> GetAll(Expression<Func<T, bool>> filter = null)
        {
            List<T> result = new List<T>();

            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    List<T> item = DeserializeList(sr.ReadToEnd());
                    result = item;
                }

                if (filter != null)
                {
                    List<T> filtred = result.AsQueryable().Where(filter).ToList();
                    return filtred;
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }


        public void Save(T item)
        {
            if (item.ID == 0)
            {
                item.ID = GetNextId();
            }

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            try
            {
                if (sr.EndOfStream)
                {
                    List<T> itemSrc = DeserializeList(sr.ReadToEnd());

                    itemSrc.Add(item);
                    string new_string = SerializeList(itemSrc);

                    sr.Close();

                    FileStream ofs = new FileStream(filePath, FileMode.OpenOrCreate);
                    StreamWriter sw = new StreamWriter(ofs);

                    sw.Write(new_string);
                    sw.Close();
                }

                else
                {
                    while (!sr.EndOfStream)
                    {
                        List<T> itemSrc = DeserializeList(sr.ReadToEnd());

                        T temp = itemSrc.FirstOrDefault(s => s.ID == item.ID);

                        if (temp == null)
                        {
                            itemSrc.Add(item);
                            string new_string = SerializeList(itemSrc);

                            sr.Close();

                            FileStream ofs = new FileStream(filePath, FileMode.OpenOrCreate);
                            StreamWriter sw = new StreamWriter(ofs);

                            sw.Write(new_string);
                            sw.Close();

                            return;
                        }
                        else
                        {
                            var index = itemSrc.IndexOf(temp);
                            itemSrc[index] = item;

                            itemSrc.RemoveAt(index);

                            itemSrc.Insert(index, item);
                            string new_string = SerializeList(itemSrc);

                            sr.Close();

                            FileStream ofs = new FileStream(filePath, FileMode.Create);
                            StreamWriter sw = new StreamWriter(ofs);

                            sw.Write(new_string);
                            sw.Close();

                            return;
                        }

                    }
                }
            }

            finally
            {
                sr.Close();
                ifs.Close();
            }
        }

        public bool Delete(T item)
        {
            bool isDeleted = false;

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            try
            {
                while (!sr.EndOfStream)
                {
                    List<T> itemSrc = DeserializeList(sr.ReadToEnd());
                    T temp = itemSrc.FirstOrDefault(i => i.ID == item.ID);

                    if (temp != null)
                    {
                        var index = itemSrc.IndexOf(temp);
                        itemSrc.RemoveAt(index);

                        isDeleted = true;
                        string new_string = SerializeList(itemSrc);

                        sr.Close();

                        FileStream ofs = new FileStream(filePath, FileMode.Create);
                        StreamWriter sw = new StreamWriter(ofs);

                        sw.Write(new_string);
                        sw.Close();

                        break;
                    }
                }
            }

            finally
            {
                sr.Close();
            }

            return isDeleted;
        }


        public string SerializeList(List<T> item)
        {
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer dataSerializer = new DataContractJsonSerializer(typeof(List<T>));
            dataSerializer.WriteObject(stream, item);
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            string result = reader.ReadToEnd();
            return result;
        }


        public List<T> DeserializeList(string json)
        {
            List<T> deserializedItem = new List<T>();
            if (!String.IsNullOrEmpty(json))
            {
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                DataContractJsonSerializer dataSerializer = new DataContractJsonSerializer(deserializedItem.GetType());
                deserializedItem = dataSerializer.ReadObject(ms) as List<T>;
                ms.Close();
            }
            return deserializedItem;
        }
    }
}
