﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repositories
{
    public class UsersToPlaylistsRepository : BaseRepository<UsersToPlaylists>
    {
        public UsersToPlaylistsRepository(string filePath) : base(filePath)
        {
        }
    }
}
