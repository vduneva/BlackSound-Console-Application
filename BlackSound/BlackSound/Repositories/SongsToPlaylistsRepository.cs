﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repositories
{
    public class SongsToPlaylistsRepository : BaseRepository<SongsToPlaylists>
    {
        public SongsToPlaylistsRepository(string filePath) : base(filePath)
        {
        }
    }
}
