﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace BlackSound.Repositories
{
    public class UsersRepository : BaseRepository<User>
    {
        public UsersRepository(string filePath) : base(filePath)
        { }
    }
}
