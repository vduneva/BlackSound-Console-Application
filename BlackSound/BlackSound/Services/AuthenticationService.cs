﻿using BlackSound.Entities;
using BlackSound.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Services
{
    public class AuthenticationService
    {
        public static User LoggedUser { get; private set; }

        public static void AuthenticateUser(string email, string password)
        {
            UsersRepository userRepo = new UsersRepository("users.txt");
            LoggedUser = userRepo.GetAll(i => i.Email == email && i.Password == password).FirstOrDefault();
        }
    }
}
