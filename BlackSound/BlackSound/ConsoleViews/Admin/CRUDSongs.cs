﻿using BlackSound.Entities;
using BlackSound.Helpers;
using BlackSound.Repositories;
using BlackSound.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ConsoleViews.Admin
{
    public class CrudSongs : Error
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();
                Console.ResetColor();
                Console.WriteLine("Songs management");
                Console.WriteLine("##################################");

                Console.WriteLine("[V]iew song");
                Console.WriteLine("[A]dd song");
                Console.WriteLine("[E]dit song");
                Console.WriteLine("[D]elete song");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine().ToUpper();

                switch (choice)
                {
                    case "A":
                        {
                            Add(0);
                            break;
                        }
                    case "E":
                        {
                            SongsRepository songRepo = new SongsRepository("songs.txt");
                            Song song = BaseView();

                            if (song != null)
                                Add(song.ID);
                            else
                                break;
                            break;
                        }
                    case "V":
                        {
                            SongsRepository songRepo = new SongsRepository("songs.txt");
                            Song song = BaseView();

                            if (song != null)
                                View(song.ID);
                            else
                                break;
                            break;
                        }
                    case "D":
                        {
                            SongsRepository songRepo = new SongsRepository("songs.txt");
                            Song song = BaseView();

                            if (song != null)
                                Delete(song.ID);
                            else
                                break;
                            break;
                        }

                    case "X":
                        {
                            return;
                        }
                    default:
                        {
                            NoInfromation();
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        public Song BaseView()
        {
            Song song = null;
            Console.Clear();
            SongsRepository songRepo = new SongsRepository("songs.txt");
            List<Song> allSongs = songRepo.GetAll();

            if (allSongs.Count == 0)
            {
                Console.WriteLine("NO SONGS");
                Console.ReadKey(true);
            }
            else
            {
                Console.WriteLine("[ID] :   Title  -------   Artist name");
                Console.WriteLine();

                foreach (Song item in allSongs)
                {
                    Console.WriteLine("[" + item.ID + "] :  " + item.Title + ".mp3  ---- " + item.ArtistName);
                    Console.WriteLine();
                }

                Console.WriteLine();
                Console.Write("Choose the song writting its [ID]: ");
                string answer = Console.ReadLine();
                int id = 0;
                bool parsed = int.TryParse(answer, out id);

                song = songRepo.GetById(id);

                if (song == null)
                {
                    Console.WriteLine("ID not found!");
                    Console.ReadKey(true);
                }
            }
            return song;
        }

        private void View(int id)
        {
            Console.Clear();
            SongsRepository songRepo = new SongsRepository("songs.txt");
            Song song = songRepo.GetById(id);

            Console.WriteLine("[ID] : " + id);
            Console.WriteLine("Title: " + song.Title);
            Console.WriteLine("Artis(s) name(s): " + song.ArtistName);
            Console.WriteLine("Year: " + song.Year);

            Console.ReadKey(true);
            return;
        }

        private void Add(int? id)
        {
            Console.Clear();
            SongsRepository songRepo = new SongsRepository("songs.txt");

            if (id == 0)
            {
                Console.WriteLine("Add new song");
                Console.WriteLine("##############################");
            }
            else
            {
                Console.WriteLine("Editing song");
                Console.WriteLine("##############################");

                Song song = songRepo.GetById(id.Value);

                if (song != null)
                {
                    //shows old information
                    Console.WriteLine("Title: " + song.Title);
                    Console.WriteLine("Artis(s) name(s): " + song.ArtistName);
                    Console.WriteLine("Year: " + song.Year);
                    Console.WriteLine();
                    Console.WriteLine("Write down the new information");
                    Console.WriteLine();
                }
            }

            SongViewModel songModel = new SongViewModel();
            Console.Write("Title:  ");
            songModel.Title = Console.ReadLine();

            Console.Write("Artist(s) Name(s): ");
            songModel.ArtistName = Console.ReadLine();

            Console.Write("Year: ");
            string answer = Console.ReadLine();
            int year = 0;
            bool parsed = int.TryParse(answer, out year);

            songModel.Year = year;

            if (id != 0)
            {
                songModel.ID = (int)id;
            }

            Validator<SongViewModel> vh = new Validator<SongViewModel>();
            bool isValid = vh.IsValid(songModel);

            //check if song is already added
            Song checkIfExist = songRepo.GetAll(i => i.Title.ToLower() == songModel.Title.ToLower() && i.ArtistName.ToLower() == songModel.ArtistName.ToLower()).FirstOrDefault();

            if (checkIfExist != null)
            {
                Console.WriteLine();
                Console.WriteLine("You've already added this song!");
                Console.ReadKey(true);
                isValid = false;
            }

            if (isValid)
            {
                //if fields are valid => save the song
                Song song = new Song();
                songModel.SetValues(song, songModel);

                songRepo.Save(song);
                Console.Clear();
                Console.WriteLine("Song added successfully");
                Console.WriteLine("Press any key to main menu");
                Console.ReadKey(true);
                return;
            }
        }

        private void Delete(int id)
        {
            SongsRepository songRepo = new SongsRepository("songs.txt");
            Song song = songRepo.GetById(id);

            SongsToPlaylistsRepository songsToPlaylistRepo = new SongsToPlaylistsRepository("songsToPlaylists.txt");
            List<SongsToPlaylists> songsToPlaylist = songsToPlaylistRepo.GetAll(i => i.SongID == song.ID);

            foreach (SongsToPlaylists item in songsToPlaylist)
            {
                songsToPlaylistRepo.Delete(item);
            }

            bool isDeleted = songRepo.Delete(song);

            Console.Clear();

            if (isDeleted)
                Console.WriteLine("Song deleted successfully!");
            else
                Console.WriteLine("Song not deleted!");

            Console.ReadKey(true);
        }
    }
}


