﻿using BlackSound.Helpers;
using BlackSound.Services;
using BlackSound.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ConsoleViews
{
    public class LoginView
    {
        public void LogIn()
        {
            while (true)
            {
                Console.Clear();
                Console.ResetColor();
                Console.WriteLine("Login");
                Console.WriteLine("#########################");

                LoginViewModel loginModel = new LoginViewModel();
                Console.Write("Email: ");
                loginModel.Email = Console.ReadLine();
                Console.Write("Password: ");
                loginModel.Password = Console.ReadLine();

                Helpers.Validator<LoginViewModel> vh = new Validator<LoginViewModel>();
                bool isValid = vh.IsValid(loginModel);

                if (isValid)
                {
                    AuthenticationService.AuthenticateUser(loginModel.Email, loginModel.Password);

                    if (AuthenticationService.LoggedUser == null)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Invalid email or/and password!");
                        Console.ReadKey(true);
                        isValid = false;
                    }
                    else if (AuthenticationService.LoggedUser.IsAdministrator == true)
                    {
                        Console.Clear();
                        Console.WriteLine("Welcome, " + AuthenticationService.LoggedUser.DisplayName + "!");
                        Console.ReadKey(true);
                        Admin.CrudSongs crud = new Admin.CrudSongs();
                        crud.Show();
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Welcome, " + AuthenticationService.LoggedUser.DisplayName);
                        Console.ReadKey(true);
                        CrudPlaylists crud = new CrudPlaylists();
                        crud.Show();
                    }
                }
            }
        }
    }
}

