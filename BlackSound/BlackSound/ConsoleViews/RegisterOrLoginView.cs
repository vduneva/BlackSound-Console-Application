﻿using BlackSound.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ConsoleViews
{
    public class RegisterOrLoginView
    {
        public void RegOrLogin()
        {
            while (true)
            {
                Console.Clear();
                Console.ResetColor();
                Console.WriteLine("Welcome to BlackSound!");
                Console.WriteLine("###########################");
                Console.WriteLine("[R]egister");
                Console.WriteLine("[L]ogin");
                Console.WriteLine();

                string choice = Console.ReadLine().ToUpper();

                //return Register's View
                if (choice == "R")
                {
                    RegisterView registerView = new RegisterView();
                    registerView.Register();
                }
                //return Login's View
                else if (choice == "L")
                {
                    LoginView loginView = new LoginView();
                    loginView.LogIn();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid choice!");
                    Console.ReadKey(true);
                }
            }
        }
    }
}
