﻿using BlackSound.Entities;
using BlackSound.Helpers;
using BlackSound.Repositories;
using BlackSound.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;


namespace BlackSound.ConsoleViews
{
    public class RegisterView
    {
        public void Register()
        {
            while (true)
            {
                Console.Clear();
                Console.ResetColor();

                RegisterViewModel registerModel = new RegisterViewModel();

                Console.WriteLine("Register");
                Console.WriteLine("############################");
                Console.WriteLine();
                Console.Write("Email: ");
                registerModel.Email = Console.ReadLine();
                Console.Write("Password: ");
                registerModel.Password = Console.ReadLine();
                Console.Write("Repeat Password: ");
                registerModel.RepeatPassword = Console.ReadLine();
                Console.Write("DisplayName: ");
                registerModel.DisplayName = Console.ReadLine();

                Helpers.Validator<RegisterViewModel> vh = new Validator<RegisterViewModel>();
                bool isValid = vh.IsValid(registerModel);

                if (isValid)
                {
                    //if model is valid => is it followed by server validation
                    Console.ForegroundColor = ConsoleColor.Red;
                    if (registerModel.Password != registerModel.RepeatPassword)
                    {
                        Console.WriteLine("Passwords do not match!");
                        isValid = false;
                        Console.ReadKey(true);
                    }

                    UsersRepository userRepo = new UsersRepository("users.txt");
                    User checkUser = userRepo.GetAll(u => u.Email == registerModel.Email).FirstOrDefault();

                    if (checkUser != null)
                    {
                        Console.WriteLine("Email is already used!");
                        isValid = false;
                        Console.ReadKey(true);
                    }

                    Console.ResetColor();

                    if (isValid)
                    {
                        //if validation is Ok => creates new user
                        User user = new User();
                        registerModel.SetValues(user, registerModel);
                        userRepo.Save(user);
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("You've registered successfully!");
                        Console.ReadKey(true);

                        Console.WriteLine();
                        LoginView loginView = new LoginView();
                        loginView.LogIn();
                    }
                }
            }
        }
    }
}
