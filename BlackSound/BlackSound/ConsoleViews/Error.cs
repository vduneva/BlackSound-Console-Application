﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ConsoleViews
{
   public class Error
    {
        protected void NoInfromation()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Error!");
            Console.WriteLine("Could not find any information about it!");

            Console.ResetColor();
            Console.WriteLine("Press any key to go back");
            return;
        }
    }
}
