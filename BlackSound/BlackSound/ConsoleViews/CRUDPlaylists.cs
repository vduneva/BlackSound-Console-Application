﻿using BlackSound.Entities;
using BlackSound.Helpers;
using BlackSound.Repositories;
using BlackSound.Services;
using BlackSound.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ConsoleViews
{
    public class CrudPlaylists : Error
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();
                Console.ResetColor();
                Console.WriteLine("Playlists management");
                Console.WriteLine("##################################");

                Console.WriteLine("[V]iew playlist");
                Console.WriteLine("[A]dd playlist");
                Console.WriteLine("[E]dit playlist");
                Console.WriteLine("[D]elete playlist");
                Console.WriteLine("[S]hare playlist");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine().ToUpper();

                switch (choice)
                {
                    case "A":
                        {
                            Add(0);
                            break;
                        }
                    case "E":
                        {
                            Console.Clear();
                            PlaylistsRepository playlistRepo = new PlaylistsRepository("playlists.txt");
                            UsersToPlaylistsRepository userToPlaylistRepo = new UsersToPlaylistsRepository("usersToPlaylists.txt");
                            List<UsersToPlaylists> loggedUserPlaylist = userToPlaylistRepo.GetAll(i => i.UserID == AuthenticationService.LoggedUser.ID);

                            List<int> playlistIdKeeperList = new List<int>();

                            if (loggedUserPlaylist.Count > 0)
                            {
                                Console.WriteLine("Edit playlist by writting its [ID]");
                                foreach (UsersToPlaylists item in loggedUserPlaylist)
                                {
                                    Playlist playlist = playlistRepo.GetAll(i => i.ID == item.PlaylistID).FirstOrDefault();
                                    Console.WriteLine("[ID] - " + playlist.ID + " :  " + playlist.Name);

                                    playlistIdKeeperList.Add(item.PlaylistID);
                                }

                                Console.WriteLine();

                                string answer = Console.ReadLine();
                                int id = 0;
                                bool parsed = int.TryParse(answer, out id);

                                Playlist choosenPlaylist = null;

                                foreach (int item in playlistIdKeeperList)
                                {
                                    if (item == id)
                                    {
                                        choosenPlaylist = playlistRepo.GetById(id);
                                        break;
                                    }
                                }

                                if (choosenPlaylist != null)
                                {
                                    Add(choosenPlaylist.ID);
                                }
                                else
                                {
                                    Console.WriteLine("ID not found!");
                                }
                            }
                            else
                            {
                                Console.WriteLine();
                                Console.WriteLine("You don't have any playlists!");
                            }
                            Console.ReadKey(true);
                            break;
                        }
                    case "V":
                        {
                            Console.Clear();
                            PlaylistsRepository playlistRepo = new PlaylistsRepository("playlists.txt");
                            UsersToPlaylistsRepository userToPlaylistRepo = new UsersToPlaylistsRepository("usersToPlaylists.txt");
                            List<UsersToPlaylists> loggedUserPlaylist = userToPlaylistRepo.GetAll(i => i.UserID == AuthenticationService.LoggedUser.ID);

                            SongsToPlaylistsRepository songsToPlaylistsRepo = new SongsToPlaylistsRepository("songsToPlaylists.txt");
                            List<Playlist> publicPlayLists = playlistRepo.GetAll(i => i.IsPublic == true);

                            List<UsersToPlaylists> userPlaylistKeeper = new List<UsersToPlaylists>();
                            List<SongsToPlaylists> songPlaylistKeeper = new List<SongsToPlaylists>();
                            List<int> playlistIdKeeperList = new List<int>();

                            UsersRepository userRepo = new UsersRepository("users.txt");

                            foreach (Playlist item in publicPlayLists)
                            {
                                List<UsersToPlaylists> notLoggedUserPlaylist = userToPlaylistRepo.GetAll(i => i.PlaylistID == item.ID && i.UserID != AuthenticationService.LoggedUser.ID);

                                userPlaylistKeeper.AddRange(notLoggedUserPlaylist);

                                foreach (UsersToPlaylists userToPlaylist in notLoggedUserPlaylist)
                                {
                                    List<SongsToPlaylists> songsToPublicPlaylist = songsToPlaylistsRepo.GetAll(i => i.PlaylistID == userToPlaylist.PlaylistID);
                                    songPlaylistKeeper.AddRange(songsToPublicPlaylist);
                                    playlistIdKeeperList.Add(userToPlaylist.PlaylistID);
                                }
                            }

                            foreach (UsersToPlaylists item in loggedUserPlaylist)
                            {
                                List<SongsToPlaylists> songsToPlaylist = songsToPlaylistsRepo.GetAll(i => i.PlaylistID == item.PlaylistID);
                                playlistIdKeeperList.Add(item.PlaylistID);
                            }

                            if (loggedUserPlaylist.Count > 0)
                            {
                                Console.WriteLine("MY PLAYLISTS");
                                Console.WriteLine("----------------------");

                                foreach (UsersToPlaylists item in loggedUserPlaylist)
                                {
                                    Playlist playlist = playlistRepo.GetAll(i => i.ID == item.PlaylistID).FirstOrDefault();
                                    Console.WriteLine("[ID] - " + playlist.ID + " :  " + playlist.Name);
                                }
                            }

                            Console.WriteLine();

                            if (userPlaylistKeeper.Count > 0)
                            {
                                Console.WriteLine("PUBLIC PLAYLISTS");
                                Console.WriteLine("----------------------");

                                foreach (UsersToPlaylists item in userPlaylistKeeper)
                                {
                                    Playlist playlist = playlistRepo.GetAll(i => i.ID == item.PlaylistID).FirstOrDefault();
                                    Console.WriteLine("[ID] - " + playlist.ID + " :  " + playlist.Name);
                                }
                            }

                            Console.WriteLine();
                            List<UsersToPlaylists> sharedToLoggedUserPlaylists = userToPlaylistRepo.GetAll(i => i.ShareToUserID.Contains(AuthenticationService.LoggedUser.ID) && i.UserID != AuthenticationService.LoggedUser.ID);

                            if (sharedToLoggedUserPlaylists.Count > 0)
                            {
                                Console.WriteLine("Shared playlists");
                                Console.WriteLine("----------------------");

                                foreach (UsersToPlaylists item in sharedToLoggedUserPlaylists)
                                {
                                    Playlist playlist = playlistRepo.GetAll(i => i.ID == item.PlaylistID).FirstOrDefault();
                                    playlistIdKeeperList.Add(item.PlaylistID);

                                    User user = userRepo.GetAll(i => i.ID == item.UserID).FirstOrDefault();
                                    Console.WriteLine("[ID] - " + playlist.ID + " :  " + playlist.Name + "         - Shared by " + user.DisplayName);
                                }
                            }

                            if (loggedUserPlaylist.Count > 0 || userPlaylistKeeper.Count > 0 || sharedToLoggedUserPlaylists.Count > 0)
                            {
                                Console.WriteLine();
                                Console.WriteLine("View playlist by writting its [ID]");
                                Console.WriteLine();

                                string answer = Console.ReadLine();
                                int id = 0;
                                bool parsed = int.TryParse(answer, out id);

                                Playlist choosenPlaylist = null;

                                foreach (int item in playlistIdKeeperList)
                                {
                                    if (item == id)
                                    {
                                        choosenPlaylist = playlistRepo.GetById(id);
                                        break;
                                    }
                                }

                                if (choosenPlaylist != null)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Playlist: " + choosenPlaylist.Name);
                                    Console.WriteLine("Description: " + choosenPlaylist.Description);
                                    Console.WriteLine("IsPublic: " + choosenPlaylist.IsPublic);
                                    Console.WriteLine();

                                    List<SongsToPlaylists> choosenSongs = songsToPlaylistsRepo.GetAll(i => i.PlaylistID == choosenPlaylist.ID);
                                    SongsRepository songRepo = new SongsRepository("songs.txt");

                                    if (choosenSongs.Count > 0)
                                    {
                                        Console.WriteLine("Song's title ******** Artist");
                                        Console.WriteLine();
                                        Console.WriteLine();

                                        foreach (SongsToPlaylists item in choosenSongs)
                                        {
                                            Song song = songRepo.GetAll(i => i.ID == item.SongID).FirstOrDefault();
                                            Console.WriteLine(song.Title + ".mp3  [" + song.ArtistName + "]");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("No songs found!");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("ID not found!");
                                }
                            }
                            else
                            {
                                Console.WriteLine("You don't have any playlist!");
                            }

                            Console.ReadKey(true);
                            break;
                        }
                    case "D":
                        {
                            Console.Clear();
                            PlaylistsRepository playlistRepo = new PlaylistsRepository("playlists.txt");
                            UsersToPlaylistsRepository userToPlaylistRepo = new UsersToPlaylistsRepository("usersToPlaylists.txt");
                            List<UsersToPlaylists> loggedUserPlaylist = userToPlaylistRepo.GetAll(i => i.UserID == AuthenticationService.LoggedUser.ID);

                            List<int> playlistIdKeeperList = new List<int>();

                            if (loggedUserPlaylist.Count == 0)
                            {
                                Console.WriteLine();
                                Console.WriteLine("You dont have any playlists!");
                                Console.ReadKey(true);
                                break;
                            }

                            SongsToPlaylistsRepository songsToPlaylistsRepo = new SongsToPlaylistsRepository("songsToPlaylists.txt");

                            foreach (UsersToPlaylists item in loggedUserPlaylist)
                            {
                                List<SongsToPlaylists> songsToPlaylist = songsToPlaylistsRepo.GetAll(i => i.PlaylistID == item.PlaylistID).ToList();
                                playlistIdKeeperList.Add(item.PlaylistID);
                            }

                            if (loggedUserPlaylist.Count != 0)
                            {
                                Console.WriteLine("Delete playlist by writting its [ID]");
                                foreach (UsersToPlaylists item in loggedUserPlaylist)
                                {
                                    Playlist playlist = playlistRepo.GetAll(i => i.ID == item.PlaylistID).FirstOrDefault();
                                    Console.WriteLine("[ID] - " + playlist.ID + " :  " + playlist.Name);
                                }

                                Console.WriteLine();

                                string answer = Console.ReadLine();
                                int id = 0;
                                bool parsed = int.TryParse(answer, out id);

                                Playlist choosenPlaylist = null;

                                foreach (int item in playlistIdKeeperList)
                                {
                                    if (item == id)
                                    {
                                        choosenPlaylist = playlistRepo.GetById(id);
                                        break;
                                    }
                                }

                                if (choosenPlaylist != null)
                                {
                                    List<SongsToPlaylists> choosenSongs = songsToPlaylistsRepo.GetAll(i => i.PlaylistID == choosenPlaylist.ID);

                                    if (choosenSongs.Count > 0)
                                    {
                                        foreach (SongsToPlaylists item in choosenSongs)
                                        {
                                            songsToPlaylistsRepo.Delete(item);
                                        }
                                    }
                                    userToPlaylistRepo.Delete(loggedUserPlaylist.Where(i => i.PlaylistID == choosenPlaylist.ID).FirstOrDefault());

                                    playlistRepo.Delete(choosenPlaylist);
                                    Console.Clear();
                                    Console.WriteLine("Playlist deleted successfully");
                                }
                                else
                                {
                                    Console.WriteLine("ID not found!");
                                }
                            }
                            Console.ReadKey(true);
                            break;
                        }
                    case "S":
                        {
                            Console.Clear();
                            Console.WriteLine("Sharing playlists");
                            Console.WriteLine("******************");
                            Console.WriteLine("MY PRIVATE PLAYLISTS");

                            PlaylistsRepository playlistRepo = new PlaylistsRepository("playlists.txt");
                            UsersToPlaylistsRepository userToPlaylistRepo = new UsersToPlaylistsRepository("usersToPlaylists.txt");
                            List<UsersToPlaylists> loggedUserPlaylist = userToPlaylistRepo.GetAll(i => i.UserID == AuthenticationService.LoggedUser.ID);
                            SongsToPlaylistsRepository songsToPlaylistsRepo = new SongsToPlaylistsRepository("songsToPlaylists.txt");

                            List<UsersToPlaylists> userPrivatePlaylistKeeper = new List<UsersToPlaylists>();
                            List<SongsToPlaylists> songPlaylistKeeper = new List<SongsToPlaylists>();

                            List<int> playlistIdKeeperList = new List<int>();
                            List<int> usersIdKeeperList = new List<int>();

                            foreach (UsersToPlaylists item in loggedUserPlaylist)
                            {
                                Playlist getPlaylistIfPrivate = playlistRepo.GetAll(i => i.ID == item.PlaylistID && i.IsPublic == false).FirstOrDefault();

                                if (getPlaylistIfPrivate != null)
                                {
                                    List<SongsToPlaylists> songsToPlaylist = songsToPlaylistsRepo.GetAll(i => i.PlaylistID == getPlaylistIfPrivate.ID);
                                    playlistIdKeeperList.Add(getPlaylistIfPrivate.ID);
                                    userPrivatePlaylistKeeper.Add(item);
                                }
                            }

                            Console.WriteLine();

                            if (userPrivatePlaylistKeeper.Count > 0)
                            {
                                Console.WriteLine("Share playlist by writting its [ID]");

                                foreach (UsersToPlaylists item in userPrivatePlaylistKeeper)
                                {
                                    Playlist playlist = playlistRepo.GetAll(i => i.ID == item.PlaylistID).FirstOrDefault();
                                    Console.WriteLine("[ID] - " + playlist.ID + " :  " + playlist.Name);
                                }

                                string answer = Console.ReadLine();
                                int id = 0;
                                bool parsed = int.TryParse(answer, out id);

                                Playlist choosenPlaylist = null;

                                foreach (int item in playlistIdKeeperList)
                                {
                                    if (item == id)
                                    {
                                        choosenPlaylist = playlistRepo.GetById(id);
                                        break;
                                    }
                                }

                                if (choosenPlaylist != null)
                                {
                                    while (true)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("You've choosen playlist [" + choosenPlaylist.Name + "] to share");
                                        Console.WriteLine();
                                        Console.WriteLine("Choose person you want share to writting its [ID]");
                                        Console.WriteLine("E[x]it");
                                        Console.WriteLine();

                                        UsersRepository userRepo = new UsersRepository("users.txt");
                                        List<User> allUsers = userRepo.GetAll(i => i.ID != AuthenticationService.LoggedUser.ID && i.IsAdministrator == false);

                                        if (allUsers.Count == 0)
                                        {
                                            Console.WriteLine("No other users found!");
                                            Console.ReadKey(true);
                                            break;
                                        }

                                        foreach (User item in allUsers)
                                        {
                                            Console.WriteLine("[" + item.ID + "]   -   " + item.DisplayName);
                                        }

                                        Console.WriteLine();

                                        string userAnswer = Console.ReadLine();
                                        int ID = 0;
                                        bool isParsed = int.TryParse(userAnswer, out ID);

                                        if (userAnswer.ToLower() == "x")
                                        {
                                            break;
                                        }

                                        User user = userRepo.GetById(ID);

                                        if (user != null)
                                        {
                                            UsersToPlaylists sharedPlaylist = userToPlaylistRepo.GetAll(i => i.PlaylistID == choosenPlaylist.ID).FirstOrDefault();

                                            if (!sharedPlaylist.ShareToUserID.Contains(user.ID))
                                            {
                                                sharedPlaylist.ShareToUserID.Add(user.ID);
                                                userToPlaylistRepo.Save(sharedPlaylist);
                                                Console.WriteLine();
                                                Console.WriteLine("Shared to user : " + user.DisplayName);
                                            }
                                            else
                                            {
                                                Console.WriteLine("You've already shared the playlist to this user");
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("ID not found!");
                                        }
                                        Console.ReadKey(true);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("ID not found!");
                                }
                            }
                            else
                            {
                                Console.WriteLine("You don't have any private playlist!");
                            }

                            Console.ReadKey(true);
                            break;
                        }
                    case "X":
                        {
                            return;
                        }
                    default:
                        {
                            NoInfromation();
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        private void Add(int? id)
        {
            Console.Clear();
            PlaylistsRepository playListRepo = new PlaylistsRepository("playlists.txt");
            SongsToPlaylistsRepository songToPlaylistRepo = new SongsToPlaylistsRepository("songsToPlaylists.txt");
            SongsRepository songRepo = new SongsRepository("songs.txt");
            UsersToPlaylistsRepository userToPlaylistRepo = new UsersToPlaylistsRepository("usersToPlaylists.txt");
            PlaylistViewModel playlistModel = new PlaylistViewModel();

            string defaultEditAnswer = "p";
            int getPlaylistID = (int)id;

            if (id == 0)
            {
                Console.WriteLine("Add new playlist");
                Console.WriteLine("##############################");
            }
            else
            {
                Console.WriteLine("Editing playlist");
                Console.WriteLine("##############################");

                Playlist existPlaylist = playListRepo.GetById(id.Value);

                if (existPlaylist != null)
                {
                    Console.WriteLine("Name: " + existPlaylist.Name);
                    Console.WriteLine("Description: " + existPlaylist.Description);
                    Console.WriteLine("IsPublic: " + existPlaylist.IsPublic);
                    Console.WriteLine();

                    List<SongsToPlaylists> choosenSongs = songToPlaylistRepo.GetAll(i => i.PlaylistID == existPlaylist.ID);

                    if (choosenSongs.Count > 0)
                    {
                        Console.WriteLine("[ID]  -- Song's title ******** Artist ");

                        foreach (SongsToPlaylists item in choosenSongs)
                        {
                            Song song = songRepo.GetAll(i => i.ID == item.SongID).FirstOrDefault();
                            Console.WriteLine("[" + song.ID + "]   " + song.Title + ".mp3  [" + song.ArtistName + "]");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No songs found!");
                        Console.ReadKey(true);
                    }

                    Console.WriteLine();
                    Console.WriteLine("Change [p]laylist information");
                    Console.WriteLine("Change [s]onglist information");
                    string answer = Console.ReadLine();

                    if (answer.ToLower() == "p")
                    {
                        Console.WriteLine();
                        Console.WriteLine("Write down the new information");
                        Console.WriteLine();
                    }
                    else if (answer.ToLower() == "s")
                    {
                        defaultEditAnswer = "s";
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                        Console.ReadKey(true);
                        return;
                    }

                    playlistModel.ID = existPlaylist.ID;
                }
            }

            if (defaultEditAnswer == "p")
            {
                Console.Write("Name: ");
                playlistModel.Name = Console.ReadLine();
                Console.Write("Description: ");
                playlistModel.Description = Console.ReadLine();
                Console.Write("IsPublic? (yes/no) :  ");
                string res = Console.ReadLine();
                if (res.ToLower() == "yes")
                    playlistModel.IsPublic = true;
                else if (res.ToLower() == "no")
                    playlistModel.IsPublic = false;

                Validator<PlaylistViewModel> vh = new Validator<PlaylistViewModel>();
                bool isValid = vh.IsValid(playlistModel);

                Playlist checkIfExist = playListRepo.GetAll(i => i.Name.ToLower() == playlistModel.Name.ToLower()).FirstOrDefault();

                if (checkIfExist != null)
                {
                    Console.WriteLine();
                    Console.WriteLine("The name is already used!");
                    Console.ReadKey(true);
                    isValid = false;
                }

                UsersToPlaylists userToPlaylist = new UsersToPlaylists();
                userToPlaylist.UserID = AuthenticationService.LoggedUser.ID;

                if (id != 0)
                {
                    userToPlaylist.ID = (int)id;
                }

                if (isValid)
                {
                    Playlist newPlaylist = new Playlist();
                    playlistModel.SetValues(newPlaylist, playlistModel);
                    playListRepo.Save(newPlaylist);

                    userToPlaylist.PlaylistID = newPlaylist.ID;
                    userToPlaylistRepo.Save(userToPlaylist);

                    Console.Clear();
                    Console.WriteLine("Playlist added successfully");
                    Console.ReadKey(true);

                    getPlaylistID = newPlaylist.ID;
                }
            }

            Playlist playlist = playListRepo.GetAll(i => i.ID == getPlaylistID).FirstOrDefault();

            if (playlist == null)
            {
                return;
            }

            Console.Clear();
            Console.WriteLine("Playlist: " + playlist.Name);
            Console.WriteLine("[A]dd songs");

            if (defaultEditAnswer == "s")
            {
                Console.WriteLine("[D]elete songs");
            }

            Console.WriteLine("E[x]it");
            Console.WriteLine();

            string choice = Console.ReadLine().ToUpper();

            switch (choice)
            {
                case "A":
                    {
                        List<Song> allSongs = songRepo.GetAll();

                        if (allSongs.Count == 0)
                        {
                            Console.WriteLine("NO SONGS");
                        }
                        else
                        {
                            while (true)
                            {
                                Console.Clear();
                                Console.WriteLine("Adding songs to playlist " + playlist.Name);

                                foreach (Song item in allSongs)
                                {
                                    Console.WriteLine("[ID] - " + item.ID + "  :  " + item.Title + "  -  " + item.ArtistName);
                                }

                                Console.WriteLine();

                                Console.WriteLine("Choose the song you want to add writting its [ID]");
                                Console.WriteLine("E[x]it");
                                string answer = Console.ReadLine().ToLower();

                                if (answer == "x")
                                {
                                    return;
                                }

                                int ID = 0;
                                bool parsed = int.TryParse(answer, out ID);

                                Song song = songRepo.GetById(ID);

                                if (song != null)
                                {
                                    SongsToPlaylists checkIfIsAlreadyAdded = songToPlaylistRepo.GetAll(i => i.SongID == song.ID && i.PlaylistID == playlist.ID).FirstOrDefault();

                                    if (checkIfIsAlreadyAdded != null)
                                    {
                                        Console.WriteLine("This song is already added to the playlist!");
                                        Console.ReadKey(true);
                                    }
                                    else
                                    {
                                        SongsToPlaylists fillThePlaylist = new SongsToPlaylists();
                                        fillThePlaylist.PlaylistID = playlist.ID;
                                        fillThePlaylist.SongID = song.ID;
                                        songToPlaylistRepo.Save(fillThePlaylist);

                                        Console.WriteLine("Song added");
                                        Console.ReadKey(true);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("ID not found!");
                                    Console.ReadKey(true);
                                }
                            }
                        }
                        break;
                    }
                case "D":
                    {
                        bool isEmpty = true;

                        while (isEmpty)
                        {
                            List<SongsToPlaylists> editSongList = songToPlaylistRepo.GetAll(i => i.PlaylistID == playlist.ID);

                            if (editSongList.Count > 0)
                            {
                                Console.Clear();
                                Console.WriteLine("Playlist: " + playlist.Name);
                                Console.WriteLine();
                                Console.WriteLine("Song's title ******** Artist");

                                foreach (SongsToPlaylists item in editSongList)
                                {
                                    Song song = songRepo.GetAll(i => i.ID == item.SongID).FirstOrDefault();
                                    Console.WriteLine("[" + song.ID + "]" + "   -----    " + song.Title + ".mp3  [" + song.ArtistName + "]");
                                }

                                Console.WriteLine();

                                Console.WriteLine("Choose the song you want to delete writting its [ID]");
                                Console.WriteLine("E[x]it");
                                Console.WriteLine();
                                string answer = Console.ReadLine().ToLower();

                                if (answer == "x")
                                {
                                    return;
                                }

                                int ID = 0;
                                bool parsed = int.TryParse(answer, out ID);

                                bool found = false;

                                foreach (SongsToPlaylists item in editSongList)
                                {
                                    if (item.SongID == ID)
                                    {
                                        songToPlaylistRepo.Delete(item);
                                        Console.WriteLine("Song deleted successfully");
                                        found = true;
                                        break;
                                    }
                                }


                                if (found == false)
                                {
                                    Console.WriteLine("ID not found!");
                                    Console.ReadKey(true);
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("No songs found!");
                                isEmpty = false;
                                return;
                            }
                        }

                        Console.ReadKey(true);
                        break;
                    }

                case "X":
                    {
                        return;
                    }
                default:
                    {
                        NoInfromation();
                        Console.ReadKey(true);
                        break;
                    }
            }
        }
    }
}



