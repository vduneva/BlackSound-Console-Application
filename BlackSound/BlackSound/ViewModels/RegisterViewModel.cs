﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;



namespace BlackSound.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Please fill up the Email field")]
        // have to put expression for email
        public string Email { get; set; }

        [Required(ErrorMessage = "Please fill up the Password field")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Password must be between 6 and 50 symbols")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please fill up the Repeat Password field")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Repeat password must be between 6 and 50 symbols")]
        [DataType(DataType.Password)]
        public string RepeatPassword { get; set; }

        [Required(ErrorMessage = "Please fill up the DispalyName field")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Display name  must be between 6 and 50 symbols")]
        public string DisplayName { get; set; }

        public bool IsAdministrator { get; set; }



        public RegisterViewModel(User item)
        {
            this.ID = item.ID;
            this.Email = item.Email;
            this.Password = item.Password;
            this.DisplayName = item.DisplayName;
            this.IsAdministrator = item.IsAdministrator;
        }

        public void SetValues(User item, RegisterViewModel model)
        {
            item.Email = model.Email;
            item.Password = model.Password;
            item.DisplayName = model.DisplayName;
            item.IsAdministrator = false;
        }

        public RegisterViewModel()
        { }
    }
}
