﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Please fill up the Email field")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Please fill up the Password field")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
