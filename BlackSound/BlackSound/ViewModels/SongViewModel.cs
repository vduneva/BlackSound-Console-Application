﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ViewModels
{
    public class SongViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Please fill up the Title field")]
        [StringLength(90, MinimumLength = 1, ErrorMessage = "Тitle must be between 90 and 1 symbols")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please fill up the Artist name field")]
        [StringLength(90, MinimumLength = 1, ErrorMessage = "Artist name must be between 90 and 1 symbols")]
        public string ArtistName { get; set; }

        [Required(ErrorMessage = "Please fill up the Year field  that has to be between 1970 and 2017")]
        [Range(1970, 2017)]
        public int Year { get; set; }

        public SongViewModel(Song item)
        {
            this.ID = item.ID;
            this.Title = item.Title;
            this.ArtistName = item.ArtistName;
            this.Year = item.Year;
        }

        public void SetValues(Song item, SongViewModel model)
        {
            item.ID = model.ID;
            item.Title = model.Title;
            item.ArtistName = model.ArtistName;
            item.Year = model.Year;
        }

        public SongViewModel()
        { }
    }
}
