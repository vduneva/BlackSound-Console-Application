﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.ViewModels
{
    public class PlaylistViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Please fill up the Name field")]
        [StringLength(90, MinimumLength = 1, ErrorMessage = "Name  must be between 90 and 1 symbols")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please fill up the Description field")]
        [StringLength(450, MinimumLength = 1, ErrorMessage = "Description  must be between 450 and 1 symbols")]
        public string Description { get; set; }

        public bool IsPublic { get; set; }

        public PlaylistViewModel(Playlist item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
            this.Description = item.Description;
            this.IsPublic = item.IsPublic;
        }

        public void SetValues(Playlist item, PlaylistViewModel model)
        {
            item.ID = model.ID;
            item.Name = model.Name;
            item.Description = model.Description;
            item.IsPublic = model.IsPublic;
        }

        public PlaylistViewModel()
        { }
    }
}
