﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BlackSound.Entities
{
    [DataContract]
    public class BaseEntity
    {
        [DataMember]
        public int ID { get; set; }
    }
}
