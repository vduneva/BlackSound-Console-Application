﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entities
{
    public class UsersToPlaylists : BaseEntity
    {
        public int UserID { get; set; }
        public int PlaylistID { get; set; }
        public List<int> ShareToUserID { get; set; }

        public UsersToPlaylists()
        {
            ShareToUserID = new List<int>();
        }
    }
}
